from django.db import models
from django.forms import ModelForm

# Create your models here.

class Car(models.Model):
    car_name = models.CharField(max_length=30)
    user_name = models.CharField(max_length=30)
    year = models.CharField(max_length=4)
    city = models.CharField(max_length=30)
    price = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.car_name

class Transaction(models.Model):
    user_name = models.CharField(max_length=30)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.user_name

class Article(models.Model):
    title = models.CharField(max_length=30)
    date = models.DateField()
    short_desc = models.TextField(default="article")
    content = models.TextField()
    photo = models.ImageField(upload_to='images/', default= 'images/test.jpg')

    def __str__(self):
        return self.title


class Review(models.Model):
    user_name = models.CharField(max_length=30)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    message = models.TextField()
    date = models.DateField()
    RATING_CHOICES = [
        ('1', '1/5'),
        ('2', '2/5'),
        ('3', '3/5'),
        ('4', '4/5'),
        ('5', '5/5'),
    ]
    rating = models.CharField(max_length=1, choices=RATING_CHOICES)

    def __str__(self):
        return self.user_name