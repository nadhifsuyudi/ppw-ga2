from django import forms
from django.forms import TextInput
from .models import Car, Transaction, Article, Review

class AddCarForm(forms.ModelForm): #TO DO
    
    class Meta:
        model = Car
        fields = [
            'car_name', 
            'user_name',
            'year',
            'city',
            'price',
            'description'
        ]
        labels = {
            'car_name': 'Car Name',
            'user_name': 'User Name',
            'year': 'Year',
            'city': 'City',
            'price': 'Price per Day',
            'description': 'Description'
        }
        widgets = {
            'car_name': forms.TextInput(attrs={'placeholder': 'Name of the car'}),
            'user_name': forms.TextInput(attrs={'placeholder': 'Your user name'}),
            'year': forms.TextInput(attrs={'placeholder': 'Manufacture year'}),
            'city': forms.TextInput(attrs={'placeholder': 'Location of the car'}),
            'price': forms.TextInput(attrs={'placeholder': 'Rate of rent per day'}),
            'description': forms.TextInput(attrs={'placeholder': 'Short description of your car'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

class DateInput(forms.DateInput):
    input_type = 'date'

class RentCarForm(forms.ModelForm): #TO DO

    class Meta:
        model = Transaction
        fields = [
            'user_name',
            'car',
            'start_date',
            'end_date',
        ]
        labels = {
            'user_name': 'User Name',
            'car': 'Car',
            'start_date': 'Start Rent Date',
            'end_date': 'End Rent Date'
        }
        widgets = {
            'user_name': forms.TextInput(attrs={'placeholder': 'Your user name'}),
            'car': forms.Select(attrs={'class':'form-control'}),
            'start_date': DateInput(),
            'end_date': DateInput(),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })