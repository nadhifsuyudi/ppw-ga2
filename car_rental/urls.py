from django.urls import include, path
from django.contrib import admin
from . import views
from django.conf import settings
from django.conf.urls.static import static

app_name = 'car_rental'

urlpatterns = [
    path('', views.index, name='index'),
    path('car/', views.car, name='car'),
    path('addCar/', views.addCar, name='addCar'),
    path('transaction/', views.transaction, name='transaction'),
    path('article/', views.article, name='article'),
    path('articleView/', views.articleView, name='articleView'),
    path('carSearch/', views.carSearch, name='carSearch'),
    path(r'^viewCar/(?P<pk>\d+)/$', views.viewCar, name='viewCar'),
    path('rentCar/', views.rentCar, name='rentCar'),
]