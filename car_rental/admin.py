from django.contrib import admin
from .models import Car, Transaction, Article, Review

# Register your models here.
admin.site.register(Car)
admin.site.register(Transaction)
admin.site.register(Article)
admin.site.register(Review)