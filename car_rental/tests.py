from django.test import TestCase, Client, SimpleTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from . import views, models
from .views import *
from .models import *

class HomePageTests(SimpleTestCase):

    def test_url_index_exist(self):
        response= self.client.get('')
        self.assertEqual(response.status_code,200)

    def test_home_page_does_not_contain_incorrect_html(self):
        response = self.client.get('')
        self.assertNotContains(response, 'Whoops Wrong page.')

class RentalTests(TestCase):

    def test_articel_exist(self):
        response= self.client.get('/article')
        self.assertEqual(response.status_code,301)

    def test_article(self):
        artic = Article.objects.create(title='hehehe', date='2020-03-18', short_desc='ehfa', content='leol', photo='gaor.png')
        self.assertTrue(artic.__str__(), artic.title)
        self.assertTrue(isinstance(artic, Article))
        count_artic = Article.objects.all().count()
        self.assertEqual(count_artic, 1)

    def test_home_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_car_page_url_is_exist(self):
        response = Client().get('/car/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'car.html')

    def test_addCar_page_url_is_exist(self):
        response = Client().get('/addCar/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addCar.html')


    def test_transaction_page_url_is_exist(self):
        response = Client().get('/transaction/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'transaction.html')

    def test_article_page_url_is_exist(self):
        response = Client().get('/article/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'article.html')

    ######################## check function ########################

    def test_home_page_resolves(self):
        url = reverse('car_rental:index')
        self.assertEquals(resolve(url).func, index)

    def test_car_page_resolves(self):
        url = reverse('car_rental:car')
        self.assertEquals(resolve(url).func, car)

    def test_addCar_page_resolves(self):
        url = reverse('car_rental:addCar')
        self.assertEquals(resolve(url).func, addCar)

    def test_viewCar_page_resolves(self):
        url = reverse('car_rental:index')