from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import redirect

from .models import Car, Article, Transaction, Review
from . import forms
from .forms import AddCarForm, RentCarForm
import re

# Create your views here.
def index(request):
    car = Car.objects.all()
    response = {'car' : car}
    return render(request, 'index.html', response)

def car(request):
    car = Car.objects.all()
    response = {'car' : car}
    return render(request, 'car.html', response)

def carSearch(request):
    carTarget = request.POST['search']
    cars = Car.objects.all()
    pattern = re.compile(carTarget, re.IGNORECASE)
    car = filter(lambda car: re.search(pattern, car.car_name), cars)
    response = {'car' : car}
    return render(request, 'carSearch.html', response)

def viewCar(request, pk):
    car = Car.objects.get(id=pk)
    reviews = Review.objects.all()
    response = {
        'car' : car,
        'reviews': reviews
    }
    return render(request, 'viewCar.html', response)


def addCar(request):
    if request.method == 'POST':
        form = forms.AddCarForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('car_rental:car')
    else:
        form = forms.AddCarForm()
        return render(request, 'addCar.html', {'form': form})

def rentCar(request):
    transaction = Transaction.objects.order_by('-date')
    car = Car.objects.all()

    if request.method == 'POST':
        form = forms.RentCarForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('../transaction')

    else:
        form = forms.RentCarForm()
    return render(request, 'rentCar.html', {'transaction': transaction, 'form': form})

def transaction(request):
    car = Car.objects.all()
    transaction = Transaction.objects.all().order_by('-date')
    response = {'car' : car, 'transaction': transaction}
    return render(request, 'transaction.html', response)

def article(request):
    artic = Article.objects.all()
    response = {'article': artic}
    return render(request, 'article.html', response)

def articleView(request):
    arti_title = request.POST['title']
    articles = Article.objects.all()
    for article in articles:
        if arti_title == article.title:
            articleTarget = article
    response = {'article' : articleTarget}
    return render(request, 'articleView.html', response)
