# Generated by Django 3.0.2 on 2020-03-06 16:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car_rental', '0002_auto_20200306_1218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='photo',
            field=models.ImageField(default='images/test.jpg', upload_to='images/'),
        ),
    ]
